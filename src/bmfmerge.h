#pragma once

#include "bmfont.h"

namespace bmfm
{

/*
  Take 2 BMFontDocument as input (a,b). Merge them as one.
  Be sure to call SaveToXML() of the returned BMFontDocument
  to preserve the merged BMFont xml file to disk. And remember
  to call SaveToPNG() of each atlas (should be only 1) in the 
  AtlasMap of the returned BMFontDocument to preserve the merged
  atlas image to disk.
 */
BMFontDocument MergeBMFonts(
	const BMFontDocument& a, const BMFontDocument& b,
	unsigned int atlasW = 2048, unsigned int atlasH = 2048);

}
