#ifdef USE_VLD
#  include <vld.h>
#endif

#include "bmfmerge.h"
#include "utils.h"
#include "atlas.h"
#include <MaxRectsBinPack.h>
#include <map>
#include <cassert>
#include <stdlib.h>

static unsigned long long _gen_key(unsigned int a, unsigned int b)
{
	unsigned long long key = (unsigned long long)a;
	key <<= 32;
	key |= ((unsigned long long)b & 0xffffffff);
	return key;
}

bmfm::BMFontDocument bmfm::MergeBMFonts(
	const bmfm::BMFontDocument& a, 
	const bmfm::BMFontDocument& b,
	unsigned int atlasW, unsigned int atlasH)
{
	// Init the packing algorithm
	rbp::MaxRectsBinPack pack(atlasW, atlasH, false);

	// Build input rect sizes for the algorithm.
	std::vector<rbp::RectSize> input_rectsizes;
	auto totalcharcnt = a.CharMap.size() + b.CharMap.size();
	input_rectsizes.reserve(totalcharcnt);

	// Note: Always using spacing and padding settings in a.
	//       So it will not work if a and b has different
	//       spacing and padding settings.
	unsigned short spacingX = a.InfoData.spacing[0];
	unsigned short spacingY = a.InfoData.spacing[1];
	unsigned short paddingU = a.InfoData.padding[0];
	unsigned short paddingR = a.InfoData.padding[1];
	unsigned short paddingD = a.InfoData.padding[2];
	unsigned short paddingL = a.InfoData.padding[3];

	for (auto it1 = a.CharMap.begin(); it1 != a.CharMap.end(); ++it1)
	{
		input_rectsizes.emplace_back(
			rbp::RectSize{
				it1->second.width + spacingX + paddingL + paddingR, 
				it1->second.height + spacingY + paddingU + paddingD });
	}
	for (auto it2 = b.CharMap.begin(); it2 != b.CharMap.end(); ++it2)
	{
		input_rectsizes.emplace_back(
			rbp::RectSize{
				it2->second.width + spacingX + paddingL + paddingR,
				it2->second.height + spacingY + paddingU + paddingD });
	}

	// Get the results from algorithm
	std::vector<rbp::Rect> output_rectsizes;
	output_rectsizes.reserve(totalcharcnt);
	pack.Insert(input_rectsizes, output_rectsizes, 
		rbp::MaxRectsBinPack::RectBestShortSideFit);
	if (output_rectsizes.size() < totalcharcnt)
	{
		logerr("Error: Unable to pack all gylph within given WxH. Try enlarge them.");
		::abort();
	}

	logfmt("Info: Packed occupancy: %.2f%%\n", pack.Occupancy() * 100.0f);

	std::multimap<unsigned long long, rbp::Rect> sorted_rects;
	for (auto it3 = output_rectsizes.begin(); it3 != output_rectsizes.end(); ++it3)
	{
		sorted_rects.insert(
			std::make_pair(_gen_key(it3->width, it3->height), *it3));
	}

	assert(sorted_rects.size() == output_rectsizes.size());

	// Prepare the merged BMFontDocument.
	// Use the same InfoData & CommonData as input a except
	// using only 1 page.
	BMFontDocument ret(a);
	ret.CommonData.pages = 1;
	ret.CommonData.scaleW = atlasW;
	ret.CommonData.scaleH = atlasH;
	ret.PageMap.clear();
	ret.CharMap.clear();
	ret.AltasMap.clear();

	ret.PageMap.insert(std::make_pair(0, BMFPageData{0, "merged_altas.png"}));
	ret.AltasMap.insert(std::make_pair(0, Atlas(atlasW, atlasH)));
	Atlas& combined_atlas = ret.AltasMap.find(0)->second;
	
	// Combine & modify BMFCharData
	for (auto it = a.CharMap.begin(); it != a.CharMap.end(); ++it)
	{
		BMFCharData cd(it->second);
		const Atlas& srcAtlas = a.AltasMap.find(cd.page)->second;
		unsigned int sx = (unsigned int)cd.x;
		unsigned int sy = (unsigned int)cd.y;

		cd.page = 0;

		auto rit = sorted_rects.find(
			_gen_key(
				cd.width + spacingX + paddingL + paddingR, 
				cd.height + spacingY + paddingD + paddingU));
		assert(rit != sorted_rects.end());
		cd.x = (unsigned short)(rit->second.x & 0xFFFF);
		cd.y = (unsigned short)(rit->second.y & 0xFFFF);
		combined_atlas.BitBlt(srcAtlas, 
			cd.x + paddingL, cd.y + paddingU, 
			sx + paddingL, sy + paddingU,
			cd.width, cd.height);
		sorted_rects.erase(rit);
		auto result = ret.CharMap.insert(std::make_pair(cd.id, cd));
		if (!result.second)
		{
			logerrfmt("Warning: Duplicated char id: %u found in BMFont face: %s.",
				cd.id, a.InfoData.facename.c_str());
		}
	}
	for (auto it = b.CharMap.begin(); it != b.CharMap.end(); ++it)
	{
		BMFCharData cd(it->second);
		const Atlas& srcAtlas = b.AltasMap.find(cd.page)->second;
		unsigned int sx = (unsigned int)cd.x;
		unsigned int sy = (unsigned int)cd.y;

		cd.page = 0;

		auto rit = sorted_rects.find(
			_gen_key(
				cd.width + spacingX + paddingL + paddingR,
				cd.height + spacingY + paddingD + paddingU));
		assert(rit != sorted_rects.end());
		cd.x = (unsigned short)(rit->second.x & 0xFFFF);
		cd.y = (unsigned short)(rit->second.y & 0xFFFF);
		combined_atlas.BitBlt(srcAtlas,
			cd.x + paddingL, cd.y + paddingU,
			sx + paddingL, sy + paddingU,
			cd.width, cd.height);
		sorted_rects.erase(rit);
		auto result = ret.CharMap.insert(std::make_pair(cd.id, cd));
		if (!result.second)
		{
			logerrfmt("Warning: Duplicated char id: %u found in BMFont face: %s.",
				cd.id, b.InfoData.facename.c_str());
		}
	}
	
	// Combine kerning settings from input b.
	for (auto it = b.KerningMap.begin(); it != b.KerningMap.end(); ++it)
	{
		auto result = ret.KerningMap.insert(std::make_pair(it->first, it->second));
		if (!result.second)
		{
			logerrfmt("Warning: Duplicated kerning data: (%u,%u) found in BMFont face: %s.",
				it->second.first, it->second.second, b.InfoData.facename.c_str());
		}
	}

	return ret;
}
