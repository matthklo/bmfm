#ifdef USE_VLD
#  include <vld.h>
#endif

#include <iostream>
#include <cassert>
#include <xgetopt.h>
#include <string>
#include <string.h>
#include "bmfmerge.h"
#include "atlas.h"
#include "utils.h"

void show_help()
{
	bmfm::logfmt(
		"Usage: \n\tbmfm [-W width] [-H height] [-n image_filename] [-x xml_filename] bmfont1 bmfont2\n\tbmfm -h\n\n"
		"bmfm takes 2 input BMFont files and merge them as a new BMFont.\n"
		"\'-W\' and \'-H\' control the merged atlas image dimensions (default is 1024).\n"
		"\'-n\' specifies the file name of the merged atlas image.\n"
		"\'-x\' specifies the file name of the merged BMFont file (in XML format).\n"
		"\'-h\' shows this message.\n");
}

int main(int argc, char* argv[])
{
	int opt;
	int atlasW = 1024;
	int atlasH = 1024;
	std::string merged_atlas_name = "merged_atlas.png";
	std::string merged_xml_name = "merged.fnt";

	while ((opt = xgetopt(argc, argv, "W:H:hn:x:")) != -1)
	{
		switch (opt)
		{
		case 'W':
			if (0 >= ::sscanf(xoptarg, "%d", &atlasW))
			{
				bmfm::logerrfmt("Error: \'%s\' is not a number.", xoptarg);
				return 1;
			}
			break;
		case 'H':
			if (0 >= ::sscanf(xoptarg, "%d", &atlasH))
			{
				bmfm::logerrfmt("Error: \'%s\' is not a number.", xoptarg);
				return 1;
			}
			break;
		case 'x':
			merged_xml_name = xoptarg;
			break;;
		case 'n':
			merged_atlas_name = xoptarg;
			break;
		default:
		case 'h':
			show_help();
			return (opt == 'h') ? 0 : 1;
		}
	}

	if (xoptind >= (argc - 1))
	{
		bmfm::logerr("Error: Insufficient input bmfont files.");
		show_help();
		return 1;
	}

	bmfm::BMFontDocument bmf1 = 
		bmfm::BMFontDocument::LoadFromXML(argv[xoptind++]);
	bmfm::BMFontDocument bmf2 =
		bmfm::BMFontDocument::LoadFromXML(argv[xoptind++]);
	bmfm::BMFontDocument bmf_merged =
		bmfm::MergeBMFonts(bmf1, bmf2, atlasW, atlasH);
	bmf_merged.PageMap[0].filename = merged_atlas_name;
	bmf_merged.AltasMap[0].SaveToPNG(merged_atlas_name);
	bmf_merged.SaveToXML(merged_xml_name);

	/*
	// Simple read/write test
	{
		bmfm::BMFontDocument d = bmfm::BMFontDocument::LoadFromXML("aaa.txt");
		d.SaveToXML("bbb.txt");
	}
	*/

	// Merge test
	/*{
		bmfm::BMFontDocument f1 = bmfm::BMFontDocument::LoadFromXML("test_org.fnt");
		bmfm::BMFontDocument f2 = bmfm::BMFontDocument::LoadFromXML("test_ch.fnt");
		bmfm::BMFontDocument f3 = bmfm::MergeBMFonts(f1, f2, 256, 256);
		f3.SaveToXML("test_merged.fnt");
		f3.AltasMap[0].SaveToPNG(f3.PageMap[0].filename);
	}*/

	/*
	// PNG save/load test
	{
		bmfm::Atlas atlas = bmfm::Atlas::LoadFromPNG("testpng.png");
		bmfm::Atlas atlas1 = bmfm::Atlas::LoadFromPNG("testpng1.png");
		bool ret = atlas.BitBlt(atlas1, 30, 30, 10, 5, 100, 100);
		assert(ret);
		atlas.SaveToPNG("testpng2.png");
	}
	*/

	return 0;
}
