## bmfm

bmfm can merge 2 bitmap font atlas (in the XML format generated by [BMFont](http://www.angelcode.com/products/bmfont/)) as one.

## Usage

bmfm [-W width] [-H height] [-n image_filename] [-x xml_filename] bmfont1 bmfont2

* -W, -H : Control the dimensions of the output bitmap (as PNG)
* -x, -n : Specify the filename of the output XML atlas description file and the bitmap
* Take 2 existing XML atlas description files of bitmap font as input.

## Limitations

* The atlas image file of both input bitmap fonts must be in PNG format. And its depth must be 32 bits (i.e. 8 bits for each RGBA channel).
* Most general settings in 'info' and 'common' tags are not merged -- they are simply inherited from bmfont1. That means you should only merge those bitmap fonts which have similar metrics.
* The merged bitmap font always contains only one page as a requirement in my usecase, while the input bitmap font may contains serveral pages.
* If a char or kerning setting exists in both fonts, use the one in bmfont1.

## Credits

* [zlib](https://zlib.net/) and [libpng](https://sourceforge.net/projects/libpng/): To read and generate PNGs
* [rapidxml](https://sourceforge.net/projects/rapidxml/files/latest/download): To parse and generate the XML atlas description files
* [RectangleBinPack](https://github.com/juj/RectangleBinPack): A versatile rectangle packing algorithm with many variants.
* [xgetopt](https://github.com/matthklo/xgetopt): A cross-platform implementation of getopt().

## License

zlib

(Applies to files placed in folders other than 'extern')